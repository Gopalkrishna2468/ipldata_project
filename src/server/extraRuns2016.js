function extraRuns2016(deliveries, matches) {

    let idsOf2016 = matches.forEach(match => {
        if (match.season === season.toString()) {
            seasonId[match.id] = match;
        }
    });


    let extraRuns = deliveries.reduce((extraRuns, currentBalls) => {
        let matchId = parseInt(currentBalls.match_id);
        if (idsOf2016.includes(matchId)) {
            if (extraRuns.hasOwnProperty(currentBalls.bowling_team)) {
                extraRuns[currentBalls.bowling_team] += parseInt(currentBalls.extra_runs);
            } else {
                extraRuns[currentBalls.bowling_team] = parseInt(currentBalls.extra_runs);
            }
        }
        return extraRuns;
    }, {})
    return extraRuns;
}

module.exports = extraRuns2016;