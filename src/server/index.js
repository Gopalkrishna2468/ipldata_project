const fs= require("fs");
const csv = require("csvtojson");
const extraRuns2016 = require("./extraRuns2016");
const MATCHES_FILE_PATH="../data/matches.csv";
const DELIVERIES_FILE_PATH="../data/matches.csv";
const JSON_OUTPUT_FILE_PATH="../public/output/extraRuns2016.json"


function main(){
csv()
    .fromFile(MATCHES_FILE_PATH)
    .then(matches =>{
        csv()
            .fromFile(DELIVERIES_FILE_PATH)
            .then(deliveries =>{
                let result = extraRuns2016(deliveries, matches);
                saveExtraRuns2016(result);
          });
        let result =extraRuns2016(matches);
        saveExtraRuns2016(result);
    });


}

function saveExtraRuns2016(result){
    const jsonData = {
        extraRunsPerTeam: result
    };
    const jsonString = JSON.stringify(jsonData);
    fs.writeFile(JSON_OUTPUT_FILE_PATH, jsonString,"utf8", err =>{
        if(err){
            console.error(err);
        }
    });
}





































/*
const fs= require("fs");
const csv = require("csvtojson");
const matchesWonPerTeam = require("./matchesWonPerTeam");
const MATCHES_FILE_PATH="../data/matches.csv";
const JSON_OUTPUT_FILE_PATH="../public/output/matchesWonPerTeam.json"


function main(){
csv()
    .fromFile(MATCHES_FILE_PATH)
    .then(matches =>{
        let result = matchesWonPerTeam(matches);
        saveMatchesWonPerTeam(result);
    });
}


function saveMatchesWonPerTeam(result){
    const jsonData = {
        matchesWonPerTeam: result
    };
    const jsonString = JSON.stringify(jsonData);
    fs.writeFile(JSON_OUTPUT_FILE_PATH, jsonString,"utf8", err =>{
        if(err){
            console.error(err);
        }
    });
}



const fs= require("fs");
const csv = require("csvtojson");
const matchesWonPerTeam = require("./matchesWonPerTeam");
const MATCHES_FILE_PATH="../data/matches.csv";
const JSON_OUTPUT_FILE_PATH="../public/output/matchesWonPerTeam.json"


function main(){
csv()
    .fromFile(MATCHES_FILE_PATH)
    .then(matches =>{
        let result = matchesWonPerTeam(matches);
        saveMatchesWonPerTeam(result);
    });
}


function saveMatchesWonPerTeam(result){
    const jsonData = {
        matchesWonPerTeam: result
    };
    const jsonString = JSON.stringify(jsonData);
    fs.writeFile(JSON_OUTPUT_FILE_PATH, jsonString,"utf8", err =>{
        if(err){
            console.error(err);
        }
    });
}


*/

main();